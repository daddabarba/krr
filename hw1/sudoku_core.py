from queue import Queue

from pysat.formula import CNF
from pysat.solvers import MinisatGH

from ortools.sat.python import cp_model
from itertools import product as cartesian

import clingo

import gurobipy as gp
from gurobipy import GRB

from itertools import product as cartesian

###
### Utils
###

# Creates a function to convert row, column pair to progressive number, given the length of the side of the sudoku
def get_rc_to_i(side):
    return lambda r, c : r*side + c

# Creates a function to convert progressive number to row, column pair, given the length of the side of the sudoku
def get_i_to_rc(side):
    return lambda i : (i//(side), i%(side))

# Creates a functon to get (partial) row of given cell (after cell) as a list of row, col tuples
def get_get_row(side):

    # Define function that returns the row of a cell (as set of constrained cells)
    return lambda r, c: list(
        zip(
            [r]*(side-c-1),
            list(range(c+1, side)),
        )
    )

# Creates a function to get (partial) column of given cell (after the cel) as a list of row, col tuples
def get_get_col(side):

    # Define function that returns the column of a cell (as set of constrained cells)
    return lambda r, c: list(
        zip(
            list(range(r+1, side)),
            [c]*(side-r-1)
        )
    )

# Creates a function to get block of given cell (after the cel) as a list of row, col tuples
def get_get_block(k):

    # Define function that returns the column of a cell (as set of constrained cells)
    def get_block(r, c):

        # Get top left corner of block
        r_block, c_block = r//k*k, c//k*k

        return filter(
            lambda x : x[0]!=r and x[1]!=c,
            cartesian(
                range(r_block, r_block+k),
                range(c_block, c_block+k)
            )
        )

    return get_block

###
### Propagation function to be used in the recursive sudoku solver
###
def propagate(sudoku_possible_values,k):

    # Init FIFO queue to keep track of cells to propagate, that is to keep track of cells with |domain| = 1 (so that the contraints, that is the standard sudoku rules, can be enforced on the domains of the cells in the same row, column, and cell)
    q = Queue()

    for i, row in enumerate(sudoku_possible_values):
        for j, cell in enumerate(row):

            # Find cells whose value is already determined
            if len(cell) == 1:
                q.put((i,j)) # Add to queue to later inforce constraints

    # Define function to enforce constraint (and update queue)
    def check(i, j, val):

        domain = sudoku_possible_values[i][j] # get domain (possible values for cell)

        if val in domain: # if val is not in domain nothing can be inferred

            domain.remove(val) # remove value from domain if there

            # check if sudoku is finished (no need to continue)
            if len(domain) < 1:
                return None

            # check if new value has been determined (|domain| = 1), if so propagate constraint (add new cell to queue)
            if len(domain) == 1:
                q.put((i,j))

        return 1

    # Loop over each item in the queue
    while not q.empty():

        # Get row and column coordinates of current cell being considered
        r, c = q.get()

        # Get value of cell at row r and column c
        val = sudoku_possible_values[r][c][0]

        # Enforce constraint in row r and column c
        for rc in range(k*k):

            # Avoid current cell (when applying column constraint), and, if so, run check function to update domains and queue
            if rc != r and (check(rc, c, val) is None):
                return sudoku_possible_values # Stop if sudoku is already broken

            # Avoid current cell (when applying row constraint), and, if so, run check function to update domains and queue
            if rc != c and (check(r, rc, val) is None):
                return sudoku_possible_values # Stop if sudoku is already broken

        # Enforce constraint in block contraining cell (r,c)

        # Get top left corner of block
        r_block, c_block = r//k*k, c//k*k

        for i in range(r_block, r_block+k):
            for j in range(c_block, c_block+k):

                # Avoid current cell, but also correspoding row and col (as they have been already checked), and, if so, run check function to update domains and queue
                if i!=r and j!=c and (check(i, j, val) is None):
                    return sudoku_possible_values # Stop if sudoku already broken

    return sudoku_possible_values;

###
### Solver that uses SAT encoding
###

def sudoku_to_cnf(sudoku, k):

    # STEP 1.1: Init Objects

    # Compute length of side of sudoku
    side = k*k

    # Init formula object
    phi = CNF()

    # For each cell at row r and column c we assign a sequential unique index i
    rc_to_i = get_rc_to_i(side)

    # For each cell i we have 9 propositions p_{i,n} = cell i has value n+1 (we start from 0). Again each gets assigned a sequential unique number
    def in_to_p(i, n):
        return i*side + n

    # STEP 1.2 Specify constraints

    # STEP 1.2.1 Specify that each cell must have a value between 1 and side, unless value is known. 

    for r, row in enumerate(sudoku):
        for c, val in enumerate(row):

            if val>0: # If value is known
                phi.append([in_to_p(i = rc_to_i(r, c), n = val)]) # Append 'Fact' or literal clause

            else: # If value is unknown, constrain it between 0 and 8

                i = rc_to_i(r,c) # Compute sequential index of cell

                # Construct clause for cell i and add it to phi. Specify that at least one value needs to be picked
                phi.append([in_to_p(i, n) for n in range(1, side+1)])

                # Ensure that only one value per cell can be picked
                for n1 in range(1, side+1):
                    for n2 in range(n1+1, side+1):
                        phi.append([-in_to_p(i, n1), -in_to_p(i, n2)])

    # STEP 1.2.2 Specify that each row needs to have different values. We want to say: for all i [ for all n [ p_{in} -> [ for all j in the same row as i [ not p_{jn} ] ] ] ] <=> for all i [ for all n [ not p_{in} v [ for all j in the same row as i [ not p_{jn} ] ] ] ] <=> for all i [ for all n [ for all j in the same row as i [ not p_{in} v not p_{jn} ] ] ] ] <=> for all i [ for all j in the same row as i [ for all n [ not p_{in} v not p_{jn} ] ] ] ]. Keep in mind that the universal quantifier is equivalent to a conjunction in all effects, thus the previous sentences is in CNF. Multiple CNF can be concatenated with a conjunction to get a formula which is also in CNF.

    # Define generalize function to apply "all diff" constraint for a subset of affected cells, given at cell at row r and column c
    def apply_constraint(get_constrained):

        # Given a collable returning the set of constrained cell by the current one at r, c (e.g. all cells in the same row), apply constraint stating that values need to be different

        for r, row in enumerate(sudoku):
            for c, val in enumerate(row):

                i = rc_to_i(r, c) # Get index of cell at r, c

                for r2, c2 in get_constrained(r, c): # Loop over other constrained cells

                    # Skip current cell
                    if r2==r and c2==c:
                        continue

                    j = rc_to_i(r2, c2) # Get sequential number of cell at r2, c2

                    if sudoku[r][c]>0: # If the value of cell i is know, simply add facts that state that the other cells in the row (column or block) cannot have the value of i
                        phi.append([-in_to_p(j, val)])

                    else: # Otherwise specify non allowed combinations (see STEP 1.2.2)

                        # Specify that no two cells can have the same value in that row (column or block)
                        for n in range(1, side+1):
                            phi.append([-in_to_p(i, n), -in_to_p(j, n)]) 


    # Apply constraint to rows
    apply_constraint(get_get_row(side))

    # Step 1.2.3 apply column alldiff constraint
    apply_constraint(get_get_col(side))

    # Step 1.2.4 apply block alldiff constraint
    apply_constraint(get_get_block(k))

    return phi

# Function to determine which clause solves the SAT problem of a given CNF formula
def solve_SAT(phi):

    solver = MinisatGH()
    solver.append_formula(phi)

    return solver.get_model() if solver.solve() else None

# Function that converts solution model for CNF to sudoku
def cnf_to_sudoku(model, k):

    side = k*k # Get length of side of sudoku

    sudoku = [ [0]*(side) for _ in range(side) ] # Init sudoku 2D list

    # Convert index of proposition to i and n
    def p_to_in(p):
        return (p-1)//side, (p-1)%side+1

    i_to_rc = get_i_to_rc(side) # Converts i to r and c

    for p in model: # Loop over propositions
        if p>0: # If the proposition is true in the given model

            i, val = p_to_in(p) # Get cell index and value

            r, c = i_to_rc(i) # Get row and column

            sudoku[r][c] = val # Set value in sudoku

    return sudoku

def solve_sudoku_SAT(sudoku,k):

    phi = sudoku_to_cnf(sudoku, k) # Compute CNF formula encoding constraints of sudoku

    solution_model = solve_SAT(phi) # Get solution from SAT solver

    # If solution is found, convert model to sudoku
    return cnf_to_sudoku(solution_model, k) if solution_model is not None else None

###
### Solver that uses CSP encoding
###

# Add set of constraints to model given sudoku
def sudoku_to_CSP(sudoku, k):

    side = k*k # Compute length of side of sudoku

    model = cp_model.CpModel() # Init CSP model

    # Init domains between 1 and side, for each cell. One variable is instanciated for each cell

    csp_vars = {} # Arrange variables in a 2D array like dict in the same fashion as the sudoku

    for r, row in enumerate(sudoku):
        for c, val in enumerate(row):

            var_name = "x_{}{}".format(r,c) # Compute name of variable (row and column dependent)

            # Add variable for each cell with domain 1..side if value is unknown, otherwise only the value is left in the domain
            csp_vars[(r,c)] = model.NewIntVar(1, side, var_name) if val<1 else model.NewIntVar(val, val, var_name)

    # Add row constraint (all values must be different for each row)

    # Define function to apply "alldiff" like constraint, for a given set of variables. get_constrained is assumed to return the constrained variables by a cell at r,c
    def apply_constraint(get_constrained):

        for r in range(side):
            for c in range(side):
                for r2, c2 in get_constrained(r, c): # Loop over every constrained cell (e.g. cells in the same row)

                    if r2==r and c2==c: # Skip current cell
                        continue

                    model.Add(csp_vars[(r,c)] != csp_vars[(r2,c2)]) # set all unique (a!=b is the same as b!=a) pair combinations to be different

    # Add all diff constrained for each cell and it's (partial, to avoid reduntant constraints a!=b and b!=a) row
    apply_constraint(get_get_row(side))

    # Add column constraint
    apply_constraint(get_get_col(side))

    # Add block constraint
    apply_constraint(get_get_block(k))

    return model, csp_vars

# Get solutuon for given csp model
def solve_CSP(model):

    solver = cp_model.CpSolver() # Init solver

    # Return function that assigns value to variable (or None if no solution is found)
    return solver if solver.Solve(model)==cp_model.FEASIBLE else None

# Construct solution sudoku from CSP solution object
def CSP_to_sudoku(csp_vars, solver, k):

    side = k*k # Get length of side of sudoku

    # Read values for each val and put in the same place in the sudoku
    return [ [ solver.Value(csp_vars[(r,c)]) for c in range(side) ] for r in range(side) ]

def solve_sudoku_CSP(sudoku,k):

    model, csp_vars = sudoku_to_CSP(sudoku, k) # Compute constraints for given sudoky

    solver = solve_CSP(model) # Solve CSP

    # Return None if CSP has no solution
    if solver is None:
        return None

    # Convert solution back to sudoku format
    return CSP_to_sudoku(csp_vars, solver, k)

###
### Solver that uses ASP encoding
###

# Constant representing FALSE conclusion
FALSE = ""

# Add set of constraints to model given sudoku
def sudoku_to_ASP(sudoku, k):

    side = k*k # Compute length of side of sudoku

    code = "" # Init ASP code

    # Define function to add fact in specific format
    def add_fact(code, fact):
        return code + "{}.\n".format(fact)

    # Define function to add if-then expressions to code
    def add_ifthen(code, conclusion, *premises):
        return code + """{} :- {}.\n""".format(conclusion, ", ".join(premises))

    # Add facts (known values)

    # Define which elements are values
    for n in range(1, side+1):
        code = add_fact(code, "isval({})".format(n))

    # Define rows and columns
    for rc in range(side):
        code = add_fact(code, "isrow(r{})".format(rc))
        code = add_fact(code, "iscol(c{})".format(rc))

    # Define blocks
    for b_id in range(side):
        code = add_fact(code, "isblock(b{})".format(b_id))

    # Add known values and assign each cell to a specific block
    for r, row in enumerate(sudoku):
        for c, val in enumerate(row):

            b_id = (r//k)*k + c//k # Get id of cell's block (unique number for each block)
            code = add_fact(code, "contains(b{}, r{}, c{})".format(b_id, r, c)) # Assign current cell to block at b_id

            if val>0: # If value is known, add it to facts
                code = add_fact(code, "hasval(r{}, c{}, {})".format(r, c, val))

    # Define that at least (and at most) one value between 1 and side needs to be in each cell. This rule is used for inference, the rest for checking (constraints)
    for n in range(1, side+1):
        code = add_ifthen(code, "hasval(R, C, {})".format(n), "isrow(R)", "iscol(C)", *["not hasval(R, C, {})".format(n1) for n1 in range(1, side+1) if n1!=n])

    # Define that two cells in the same row cannot have the same value
    code = add_ifthen(code, FALSE, "isrow(R)", "iscol(C1)", "iscol(C2)", "C1!=C2", "isval(N)", "hasval(R, C1, N)", "hasval(R, C2, N)")
    # Define that two cells in the same column cannot have the same value
    code = add_ifthen(code, FALSE, "iscol(C)", "isrow(R1)", "isrow(R2)", "R1!=R2", "isval(N)", "hasval(R1, C, N)", "hasval(R2, C, N)")
    # Define that two cells in the same block cannot have the same value (ignore row and column by setting R1!=R2 AND, instead of OR, C1!=C2, as this is handled by the two previous constraints)
    code = add_ifthen(code, FALSE, "isrow(R1)", "isrow(R2)", "R1!=R2", "iscol(C1)", "iscol(C2)", "C1!=C2", "isblock(B)", "contains(B, R1, C1)", "contains(B, R2, C2)", "isval(N)", "hasval(R1, C1, N)", "hasval(R2, C2, N)")

    # Specify that we are interested only in ternary predicate hasval
    code += """#show hasval/3.\n"""

    return code

# Get solutuon for given csp model
def solve_ASP(code):

    # Init solver with given code
    control = clingo.Control()
    control.add("base", [], code)
    control.ground([("base", [])])

    # Handle an answer set

    # Init empty list of solutions
    solution = []
    def on_model(model):
        solution.append(model.symbols(shown=True)) # Append solution

    control.configuration.solve.models = 1 # Need only one solution, so ask the solver to find 1 at most

    answer = control.solve(on_model=on_model) # Compute solution

    # Return None if KB is not satisfiable
    return solution[0] if answer.satisfiable else None

# Construct solution sudoku from CSP solution object
def ASP_to_sudoku(solution, k):

    side = k*k # Get length of side of sudoku

    sudoku = [ [0]*(side) for _ in range(side) ] # Init sudoku 2D list

    # Loop over propositions
    for p in solution:

        R, C, N = p.arguments # Extract which cell (row column coordinates) has which value

        r, c = int(str(R)[1:]), int(str(C)[1:]) # Extract row and column number from Symbol

        sudoku[r][c] = int(str(N)) # Set value in sudoku

    return sudoku

def solve_sudoku_ASP(sudoku,k):

    code = sudoku_to_ASP(sudoku, k) # Generate ASP code for current sudoku

    # we know that 
    solution = solve_ASP(code) # Compute solution (or if there is a solution) from the given HORN form KB

    # Convert to sudoku and return (if solution is found)
    return ASP_to_sudoku(solution, k) if solution is not None else None

###
### Solver that uses ILP encoding
###

def sudoku_to_ILP(sudoku, k):

    # STEP 1.1: Init Objects

    side = k*k # Compute length of side of sudoku

    model = gp.Model() # Initialize model
    ilp_vars = {} # Binary variables will be arranged in a 3D array like dict (row x col x number), representing if there is specific value for a specific cell

    # STEP 1.2 Specify constraints

    # STEP 1.2.1 Specify that each cell must have a value between 1 and side, unless value is known. 

    for r, row in enumerate(sudoku):
        for c, val in enumerate(row):

            for n in range(1, side+1):
                ilp_vars[(r,c,n)] = model.addVar(vtype=GRB.BINARY, name="x({},{},{})".format(r, c, n))

            # Specify that at most and at least one value can be picked
            model.addConstr(gp.quicksum(ilp_vars[(r,c,n)] for n in range(1,side+1)) == 1)

            if val>0:
                model.addConstr(ilp_vars[(r,c,val)] == 1)

    # STEP 1.2.2 Specify that each row needs to have different values.

    # Define generalize function to apply "all diff" constraint for a subset of affected cells, given at cell at row r and column c
    def apply_constraint(get_constrained):

        # Given a collable returning the set of constrained cell by the current one at r, c (e.g. all cells in the same row), apply constraint stating that values need to be different

        for r, row in enumerate(sudoku):
            for c, val in enumerate(row):
                for r2, c2 in get_constrained(r, c): # Loop over other constrained cells

                    # Skip current cell
                    if r2==r and c2==c:
                        continue

                    # Specify that no two cells can have the same value in that row (column or block)
                    for n in range(1, side+1):
                        model.addConstr(ilp_vars[(r,c,n)] + ilp_vars[(r2,c2,n)] <= 1) # At most one can have a value n

    # Apply constraint to rows
    apply_constraint(get_get_row(side))

    # # Step 1.2.3 apply column alldiff constraint
    apply_constraint(get_get_col(side))

    # # Step 1.2.4 apply block alldiff constraint
    apply_constraint(get_get_block(k))

    return model, ilp_vars

def solve_ILP(model):

    model.optimize()

    return model.status == GRB.OPTIMAL

def ILP_to_sudoku(ilp_vars, k):

    side = k*k # Length of the side of the sudoku

    sudoku = [ [0]*(side) for _ in range(side) ] # Init sudoku 2D list

    for r in range(side):
        for c in range(side):
            for n in range(1, side+1):
                if ilp_vars[(r,c,n)].x>0: # Collapse third one-hot encoded dimension
                    sudoku[r][c] = n
                    break

    return sudoku

def solve_sudoku_ILP(sudoku, k):

    model, ilp_vars = sudoku_to_ILP(sudoku, k)

    return ILP_to_sudoku(ilp_vars, k) if solve_ILP(model) else None
