from planning import PlanningProblem, Action, Expr, expr
import planning

import clingo

def solve_planning_problem_using_ASP(planning_problem,t_max):
    """
    If there is a plan of length at most t_max that achieves the goals of a given planning problem,
    starting from the initial state in the planning problem, returns such a plan of minimal length.
    If no such plan exists of length at most t_max, returns None.

    Finding a shortest plan is done by encoding the problem into ASP, calling clingo to find an
    optimized answer set of the constructed logic program, and extracting a shortest plan from this
    optimized answer set.

    NOTE: still needs to be implemented. Currently returns None for every input.

    Parameters:
        planning_problem (PlanningProblem): Planning problem for which a shortest plan is to be found.
        t_max (int): The upper bound on the length of plans to consider.

    Returns:
        (list(Expr)): A list of expressions (each of which specifies a ground action) that composes
        a shortest plan for planning_problem (if some plan of length at most t_max exists),
        and None otherwise.
    """

    # Init code

    code = ""

    # Problem instance definition

    # Specify which values of time are valid time-steps
    code += f'''
time(0..{t_max}).'''

    facts = {} # Keep tract of facts and add unary predicate for each new literal
    constants = {} # Keep track of constants (symbol) if FOL is used (to restrict scope of quantifiers/variables)
    actions = {} # Keep track of actions (map from name used in ASP code to original action, used for conversion from answer set to strategy)

    # Define function to link actions to either effects or preconditions
    def define_action(code, action, action_str, fact, relationtype):

        # Parse negation

        val = 1 # Assume literal is positive (not negated)
        if fact.op == '~': # if negated
            val = 0
            fact = fact.args[0] # Remove negation and remember that literal was negated

        # Process arguments (if any, in case FOL is used)
        args = [(str(arg).lower() if str(arg)[0].isupper() else str(arg).upper()) for arg in fact.args]

        # Add new constants (if a new symbol is met, store state it is a constant)
        for arg in fact.args:
            if str(arg)[0].isupper() and str(arg).lower() not in constants:
                constants[str(arg).lower()] = str(arg)

        # Convert literal to string (handle upper and lower cases conventions conversion correctly)
        fact_str = f"{str(fact.op).lower()}" + (f"({','.join(args)})" if len(fact.args)>0 else "")

        # Make precondition/effect safe if not met yet
        if fact_str not in facts:
            facts[fact_str] = str(fact.op)

        # Store fact (new literal) using unary predicate fact/1. If variables are used, then we assume this is just a short form for writing the literal with any possible value for any variable. This will be encoded by using the template fact(name(X)) :- constant(X), so that each vaiable X will be replaced (when explanding) with any possible constant (amongs the ones met so far).
        fact = fact_str
        code += f'''
fact({fact}) :- {", ".join("constant(" + arg + ")" for arg in args)}.'''

        # Get all variables present in the rule (prerequisites may have extra variables)
        variables = set([str(arg).upper() for arg in action.args if str(arg)[0].islower()] + [arg for arg in args if arg[0].isupper()])

        # Encode fact that certain action has certain precondition/effect. This is done using the predicates precon(A, F, V) and effect(A, F, V). The former indicates preconditions of actions, the latter effects. V is 1 if the literal was not negated in the precondition/effect, 0 otherwise. F is the atom (negation free literal), A the action.
        code += f'''
{relationtype}({str(action_str)}, {fact}, {val}) :- {", ".join("constant(" + var + ")" for var in variables)}.'''

        return code

    # Encode actions
    for action in planning_problem.actions:

        # Account for variables

        # For any constant symbol found, specify it as a symbol
        for arg in action.args:
            if str(arg)[0].isupper() and str(arg).lower() not in constants:
                constants[str(arg).lower()] = str(arg)

        # Make string of action (account for variables, invert the pattern that lower case letters are variables and upper case are constants)
        action_str = f"{str(action.name).lower()}{('(' + ','.join(str(arg).lower() if str(arg)[0].isupper() else str(arg).upper() for arg in action.args) + ')') if len(action.args)>0 else ''}"

        # Store str (in ASP) to action mapping for later mapping from answer set back to strategy
        actions[str(action.name).lower()] = action

        # Store fact that 'action' is an action to make it safe, if any variables are found, make them safe (bound them to constants)
        code += f'''
action({action_str}) :- {", ".join("constant(" + str(arg).upper() + ")" for arg in action.args if str(arg)[0].islower())}.'''

        # Parse preconditions
        for precon in action.precond:
            code = define_action(code, action, action_str, precon, 'precon')

        # Parse effects
        for effect in action.effect:
            code = define_action(code, action, action_str, effect, 'effect')

    # Encode initial state
    for fact in planning_problem.initial:

        # Check if new constants/symbols are introduce, if so store them for later (used both to make constant/1 predicates, but also for later conversion from answer set to strategy)
        for arg in fact.args:
            if str(arg)[0].isupper() and str(arg).lower() not in constants:
                constants[str(arg).lower()] = str(arg)

        # Convert to string
        fact_str = str(fact).lower()

        # If new literal is met add predicate for it
        if fact_str not in facts:
            code += f'''
fact({fact_str}).'''
            facts[fact_str] = str(fact.op)

        fact = fact_str

        # Set that at time-step 0 (initial state) a certain fact is true
        code += f'''
state(0, {fact}).'''

    # Encode that goal needs to be achieved
    for goal in planning_problem.goals:

        # Process negation 
        val = 1
        if hasattr(goal, 'op') and goal.op == '~':
            goal = goal.args[0]
            val = 0

        # Check if new constants/symbols are introduced, if so store them for later
        for arg in goal.args:
            if str(arg)[0].isupper() and str(arg).lower() not in constants:
                constants[str(arg).lower()] = str(arg)

        # Convert arguments to string (handle correct conversion of upper/lower cases convenstions for variables and constants)
        args = [(str(arg).lower() if str(arg)[0].isupper() else str(arg).upper()) for arg in goal.args]

        # Use the predicate goal(F, V) to indicate that literal F needs to have truth value V (1 or 0) for that goal to be achieved. Variables are treated as always (goal(name(X)) is expanded for X being any possible constant)
        code += f'''
goal({str(goal.op).lower()}{"(" + ",".join(args) + ")" if len(args)>0 else ""}, {val}) :- {", ".join("constant(" + arg + ")" for arg in args if arg[0].isupper())}.'''

    # Write constants (symbol used in problem definition when variables are encountered)
    for constant in constants:
        code += f'''
constant({constant}).'''

    # Problem class  definition

    # Define truth values (bound negated/ non negated specifications to be either 0 or 1)
    code+= f'''

truthval(0..1).
'''

    # Define which actions are available at each timestep. First check that all the preconditions that are not negated are true at time T (posprecon(T, A)), then check that all the literals that are negated are not true at time T (negprecon(T, A)). If both conditions hold then the action is available at time T
    code += '''
posprecon(T, A) :- time(T), action(A), state(T, F) : precon(A, F, 1).
negprecon(T, A) :- time(T), action(A), not state(T, F) : precon(A, F, 0).
available(T, A) :- time(T), action(A), posprecon(T, A), negprecon(T, A).
'''

    # Ensure that at each time-step one action (of the available ones) is chosen
    code += '''
1 { chosen(T, A) : available(T, A) } 1 :- time(T).
'''

    # Implement effect of delete list. If a fact was true at time-step T-1 and was not in the delete list of the action applied at time step T-1, then it will be true in the state at time T. Here we assume that if action A is chosen at time step T then the effects will be available in state T+1.
    code += f'''
state(T, F) :- time(T-1), state(T-1, F), chosen(T-1, A), not effect(A, F, 0).'''

    # Implement add list of an action. If an action was selected at time T-1 then the facts in the add list are true at time T
    code += f'''
state(T, F) :- time(T-1), chosen(T-1, A), effect(A, F, 1).
'''

    # Detect at which time-steps the goal is reached. Also ensure that the goal was not previously reached. This makes it so that in the answer set, the first time step to reach the goal is the only one that will have goalreached being true. prev/1 checks if the goal was previously matched by being true if the goal was previously matched for the previous time-step, or if the goal was reached in the previous time-step (so if goal is first reached at time T then prev will be true for T1>T). goalstrue and goalsfalse respectively check that all the literals that need to be true and false respectively, are true and false respectively at time T (simultaneously).
    code += '''
goalstrue(T) :- time(T), state(T, F) : goal(F, 1).
goalsfalse(T) :- time(T), not state(T, F) : goal(F, 0).
goalreached(T) :- not prev(T), time(T), goalstrue(T), goalsfalse(T). 
prev(T+1) :- time(T), goalreached(T).
prev(T+1) :- time(T), prev(T).
'''

    # Ensure that goal is reached, the constrain states that if at all time-steps the goal is not reached (thus if the goal is never reached), then the answer set is not valid. The minimizing statement simply minimizes the time-step at which the goal is first reached.
    code += '''
:- not goalreached(T) : time(T).
#minimize {T : goalreached(T)}.
'''

    # Show only actions at each time-step
    code += f'''
#show chosen/2.
#show goalreached/1.'''

    print(code)

    # Call solver with generated code

    control = clingo.Control()
    control.add("base", [], code)
    control.ground([("base", [])])

    # Handle an answer set

    # define function to parse clingo.Symbol into Expression
    def parse_fact(atom):

        exp = ''

        # Convert facts/operator
        exp += str(actions[str(atom.name)].name)

        # Use previously store dictonaries to convert strings back to original upper/lower cases
        if len(atom.arguments) > 0:
            exp += '(' + ','.join([constants[str(arg)] for arg in atom.arguments]) + ')'

        return expr(exp)

    # Init empty list of solutions
    solutions = []

    # In on model, the model is stored
    def on_model(model):

        # Ensure model is optimal
        if model.optimality_proven == True:

            len_sol = None # store at which time-step the goal has been reached, to cut out chosen actions after that time-step
            solution = {} # map from time-step to action

            # For each predicate
            for atom in model.symbols(shown=True):

                # Store which action was chosen at which time-step
                if atom.name == "chosen":
                    solution[int(str(atom.arguments[0]))] = atom.arguments[1]
                # Store at which time-step the goal was first met
                else:
                    len_sol = int(str(atom.arguments[0]))

            # get only the first len_sol actions (and process them correctly into Expr objects)
            solutions.append([parse_fact(solution[T]) for T in range(len_sol)])

    control.configuration.solve.opt_mode = "optN" # Get optimal answer sets
    control.configuration.solve.models = 1 # Need only one solution, so ask the solver to find 1 at most

    control.solve(on_model=on_model) # Compute solution

    # Check if a solution was found
    if len(solutions) == 0:
        return None

    # Return processed solution
    return solutions[0]
 
