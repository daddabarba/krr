#! /bin/bash

echo Runnin Trivial 0
python3 asp_planner.py --i inputs/trivial0.planning
echo Running Trivial 1
python3 asp_planner.py --i inputs/trivial1.planning

echo Running Easy 0
python3 asp_planner.py --i inputs/easy0.planning
echo Running Easy 1
python3 asp_planner.py --i inputs/easy1.planning
echo Running Easy 2
python3 asp_planner.py --i inputs/easy2.planning
echo Running Easy 3
python3 asp_planner.py --i inputs/easy3.planning
echo Running Easy 4
python3 asp_planner.py --i inputs/easy4.planning

echo Running Medium 0
python3 asp_planner.py --i inputs/medium0.planning
echo Running Medium 1
python3 asp_planner.py --i inputs/medium1.planning
echo Running Medium 2
python3 asp_planner.py --i inputs/medium2.planning

echo Running No Sol 0
python3 asp_planner.py --i inputs/nosol0.planning
echo Running No Sol 1
python3 asp_planner.py --i inputs/nosol1.planning
