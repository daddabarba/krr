node(1..4).

edge(1,2).
edge(1,3).
edge(1,4).
edge(2,3).
edge(2,4).
edge(3,4).

b(X) :- node(X), not r(X), not g(X).
r(X) :- node(X), not b(X), not g(X).
g(X) :- node(X), not r(X), not b(X).

b(X); r(X); g(X) :- node(X).
n :- node(X), node(Y), edge(X,Y), r(X), r(Y).
n :- node(X), node(Y), edge(X,Y), g(X), g(Y).
n :- node(X), node(Y), edge(X,Y), b(X), b(Y).

r(X) :- n, node(X).
g(X) :- n, node(X).
b(X) :- n, node(X).

:- node(X), r(X), g(X).
:- node(X), r(X), b(X).
:- node(X), g(X), b(X).

#show r/1.
#show g/1.
#show b/1.
