isprop(p).
isprop(q).
isprop(r).
isprop(s).

val(p, 1).
val(q, 1).

isdefault(d1).
isdefault(d2).

haspremise(d1, p, 1).
hasjustification(d1, r, 1).

haspremise(d2, p, 1).
haspremise(d2, r, 1).
hasjustification(d2, s, 0).
hasjustification(d2, q, 1).

istruthval(1).
istruthval(0).

applicable(D, 0) :- isdefault(D), not applicable(D, 1).
applicable(D, 1) :- isdefault(D), not applicable(D, 0).

applicable(D, 0) :- isdefault(D), isprop(X), istruthval(V), haspremise(D, X, V), not val(X, V).
applicable(D, 0) :- isdefault(D), isprop(X), istruthval(V1), istruthval(V2), V1!=V2, hasjustification(D, X, V1), val(X, V2).

val(X, V) :- isdefault(D), isprop(X), istruthval(V), applicable(D, 1), hasjustification(D, X, V).

:- isprop(X), val(X, 1), val(X, 0).
:- isdefault(D), applicable(D, 0), applicable(D, 1).

:- not val(s, 0).

#show val/2.
#show applicable/2.
