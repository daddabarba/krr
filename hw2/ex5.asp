% Define atoms
atom(a).
atom(b).
atom(c).

% Define sentences
sent(phi1).
sent(phi2).

% Assign clauses to sentences
clause(phi1, p1c1).
clause(phi1, p1c2).
clause(phi1, p1c3).

clause(phi2, p2c1).
clause(phi2, p2c2).
clause(phi2, p2c3).
clause(phi2, p2c4).

% Assign atoms (with negative or positive value) to clauses
literal(p1c1, a, 1).
literal(p1c1, b, 1).
literal(p1c2, a, 1).
literal(p1c2, c, 1).
literal(p1c3, a, 0).

literal(p2c1, a, 1).
literal(p2c1, b, 1).
literal(p2c2, a, 1).
literal(p2c2, c, 1).
literal(p2c3, a, 0).
literal(p2c4, c, 0).

% Define truth values
truthval(1).
truthval(0).

% Generate all cobinations
val(S, A, 1); val(S, A, 0) :- atom(A), sent(S).

% Check if the combination of values, makes phi2 false

% Define what makes a clause true
val(S, C, 1) :- sent(S), clause(S, C), atom(A), truthval(V), literal(C, A, V), val(S, A, V).
val(S, C, 0) :- sent(S), clause(S, C), val(S, A, 1-V) : sent(S), clause(S, C), truthval(V), literal(C, A, V). 

% Define what makes a CNF fomrula false
val(S, S, 0) :- sent(S), clause(S, C), val(S, C, 0).
val(S, S, 1) :- sent(S), not val(S, S, 0).

% Check property
prop :- val(phi2, phi2, 0).

% Saturate if unsatisfied
val(phi2, A, V) :- sent(S), atom(A), truthval(V), prop.

% Ensure failure if phi2 is satisfiable
:- not prop.

% Leave out only solutions where phi1 is true (if it is not satisfiable there won't be any)
:- val(phi1, phi1, 0).

#show val/3.

