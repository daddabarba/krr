#! /bin/bash

U=(5 6 7 8 9 10 15)
M=(4 5 6 9 9 9 12)
N=(2 3 4 6 6 6 5)

for i in $(seq 0 $(expr ${#U[@]} - 1))
do
    echo Run $i

    cp ex4.asp ex4.asp.cp
    cp ex4_baseline.asp ex4_baseline.asp.cp

    echo '#const u='${U[$i]}. | cat - ex4.asp > temp; mv temp ex4.asp
    echo '#const u='${U[$i]}. | cat - ex4_baseline.asp > temp; mv temp ex4_baseline.asp

    echo '#const m='${M[$i]}. | cat - ex4.asp > temp; mv temp ex4.asp
    echo '#const m='${M[$i]}. | cat - ex4_baseline.asp > temp; mv temp ex4_baseline.asp

    echo '#const n='${N[$i]}. | cat - ex4.asp > temp; mv temp ex4.asp
    echo '#const n='${N[$i]}. | cat - ex4_baseline.asp > temp; mv temp ex4_baseline.asp

    python3 ASP_interpreter.py ex4_baseline.asp | sort > out_baseline.txt
    python3 ASP_interpreter.py ex4.asp | sort > out.txt 

    diff out_baseline.txt out.txt

    mv ex4.asp.cp ex4.asp
    mv ex4_baseline.asp.cp ex4_baseline.asp
    rm out.txt
    rm out_baseline.txt
 done

