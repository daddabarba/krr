atom(a).
atom(b).
atom(c).
val(A, 1) :- atom(A), not val(A, 0).
val(A, 0) :- atom(A), not val(A, 1).

compatom(x).
val(x, 1) :- val(b, 1), val(c, 1).
val(x, 0) :- val(b, 0).
val(x, 0) :- val(c, 0).
compatom(y).
val(y, 1) :- val(a, 1).
val(y, 1) :- val(x, 1).
val(y, 0) :- val(a, 0), val(x, 0).
compatom(j).
val(j, 1) :- val(a, 1).
val(j, 1) :- val(c, 1).
va(j, 0) :- val(a, 0), val(c, 0).

compatom(phi).
val(phi, 1) :- val(y, 1), val(j, 0).
val(phi, 0) :- val(y, 0); val(j, 1).

:- not val(phi, 1).

:- compatom(X), val(X, 1), val(X, 0).
:- atom(X), val(X, 1), val(X, 0).

#show val/2.
