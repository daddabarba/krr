import clingo
from argparse import ArgumentParser

def main(args):

    with open(args.source, "rt") as f:
        code = "".join(f.readlines())

    if args.h:
        print("########\n# CODE #\n########\n")
        print(code)
        print("########\n# CODE #\n########\n")

    # Init solver with given code
    control = clingo.Control()
    control.add("base", [], code)
    control.ground([("base", [])])

    # Handle an answer set

    # Init empty list of solutions
    solutions = []
    def on_model(model):
        solutions.append(model.symbols(shown=True)) # Append solution

    control.configuration.solve.models = args.n_sol # Need only one solution, so ask the solver to find 1 at most

    answer = control.solve(on_model=on_model) # Compute solution

    # Return None if KB is not satisfiable
    if args.h:
        print("\n############\n# SOLUTION #\n############\n")
    if not answer.satisfiable:
        print("Not Satisfiable")
    else:
        for solution in solutions:
            print(solution)
    if args.h:
        print("\n############\n# SOLUTION #\n############\n")

if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument(
        "source",
        metavar="file",
        type = str,
        help = "Name of file with ASP source code"
    )

    parser.add_argument(
        "--n_sol",
        "-n",
        type = int,
        default = 0,
        help = "Number of solution to find"
    )

    parser.add_argument(
        "--h",
        action = 'store_true',
        help = "Human readable output"
    )

    main(parser.parse_args())

