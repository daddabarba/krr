a :- not af.
af :- not a.
b :- not bf.
bf :- not b.
c :- not cf.
cf :- not c.

:- not a, not b.
:- not a, not c.

:- a.
:- c.
