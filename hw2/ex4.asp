#const n=2.
#const m=3.
#const u=4.

element(1..u).
isnumber(0..u).

item(X) :- element(X), not notitem(X).
notitem(X) :- element(X), not item(X).

ctr(I, K+1) :- ctr(I+1, K), item(I), element(I), isnumber(K).
ctr(I, K) :- ctr(I+1, K), element(I), isnumber(K).

ctr(u+1, 0).

:- element(X), item(X), notitem(X).

:- not ctr(1, n).
:- ctr(1, m+1).

#show item/1.
